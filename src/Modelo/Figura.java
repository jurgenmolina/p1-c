/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *Clase figura
 * @author madar
 */
public class Figura {
    
    //Ejemplo: Estrella, círculo, cuadrado, etc...
    private String nombreFigura;

    public String getNombreFigura() {
        return nombreFigura;
    }

    public void setNombreFigura(String nombreFigura) {
        this.nombreFigura = nombreFigura;
    }

    public Figura(String nombreFigura) {
        this.nombreFigura = nombreFigura;
    }

    public Figura() {
    }
    
    
}
